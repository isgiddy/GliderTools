# -*- coding: utf-8 -*-
#!/usr/bin/env python
from __future__ import (print_function as _pf,
                        unicode_literals as _ul,
                        absolute_import as _ai)


def grid_data(x, y, var, bins=None, how='mean', interp_lim=6, as_xarray=False, verbose=True):
    """
    Grids the input variable to bins for depth/dens (y) and time/dive (x).
    The bins can be specified to be non-uniform to adapt to variable sampling
    intervals of the profile. It is useful to use the ``gt.plot.bin_size``
    function to identify the sampling intervals. The bins are averaged (mean) by
    default but can also be the ``median, std, count``,

    Parameters
    ----------
    x : np.array, dtype=float, shape=[n, ]
        The horizontal values by which to bin need to be in a psudeo discrete
        format already. Dive number or ``time_average_per_dive`` are the
        standard inputs for this variable. Has ``p`` unique values.
    y : np.array, dtype=float, shape=[n, ]
        The vertical values that will be binned; typically depth, but can also
        be density or any other variable.
    bins : np.array, dtype=float; shape=[q, ], default=[0 : 1 : max_depth ]
        Define the bin edges for y with this function. If not defined, defaults
        to one meter bins.
    how : str, defualt='mean'
        the string form of a function that can be applied to pandas.Groupby
        objects. These include ``mean, median, std, count``.
    interp_lim : int, default=6
        sets the maximum extent to which NaNs will be filled.
    as_xarray : bool, default=False
        if True, will return an ``xarray.DataArray`` section of the data
        if False, will return ``pandas.DataFrame`` section of the data

    Returns
    -------
    glider_section : pandas.DataArray / xarray.DataArray, shape=[p, q]
        A 2D section in the format specified by ``ax_xarray`` input.

    Raises
    ------
    Userwarning
        Triggers when ``x`` does not have discrete values.
    """
    from pandas import cut, Series
    from numpy import abs, arange, array, c_, nanmax, unique, diff, nanmean
    import warnings

    z = Series(var)
    y = array(y)
    x = array(x)

    u = unique(x).size
    s = x.size
    if (u / s) > 0.2:
        raise UserWarning(
            'The x input array must be psuedo discrete (dives or dive_time). '
            '{:.0f}% of x is unique (max 20% unique)'.format(u / s * 100))

    bins = arange(nanmax(y) + 1) if bins is None else bins

    # warning if bin average is smaller than average bin size
    if verbose:
        avg_bin_size = diff(bins).mean()
        avg_depth_bined_sampling_freq = []
        dbins = 100
        for d in arange(0, y.max()+1, dbins):
            d0, d1 = d, d+100
            i = (y > d0) & (y < d1)
            avg_depth_bined_sampling_freq += abs(diff(y[i])).mean(),
        avg_depth_bined_sampling_freq = nanmean(avg_depth_bined_sampling_freq)
        print(("Mean bin size = {:.2f}\n"
            "Mean depth binned ({} m) vertical sampling frequency = {:.2f}"
            ).format(avg_bin_size, dbins, avg_depth_bined_sampling_freq))

    labels = c_[bins[:-1], bins[1:]].mean(axis=1)
    bins = cut(y, bins, labels=labels)

    grp = Series(z).groupby([x, bins])
    grp_agg = getattr(grp, how)()
    gridded = grp_agg.unstack(level=0)
    gridded = gridded.reindex(labels.astype(float))

    if interp_lim > 0:
        gridded = gridded.interpolate(limit=interp_lim).bfill(limit=interp_lim)

    if as_xarray:
        return gridded.stack().to_xarray()
    return gridded


def time_average_per_dive(dives, time,
                          time_units='seconds since 1970-01-01 00:00:00'):
    """
    Gets the average time stamp per dive. This is used to create psuedo discrete
    time steps per dive for plotting data (using time as x-axis variable).

    Parameters
    ----------
    dives : np.array, dtype=float, shape=[n, ]
        discrete dive numbers (down = d.0; up = d.5) that matches time length
    time : np.array, dtype=datetime64, shape=[n, ]
        time stamp for each observed measurement
    time_units : str
        time_units if `time` is not in datetime64[ns] units

    Returns
    -------
    time_average_per_dive : np.array, dtype=datetime64, shape=[n, ]
        each dive will have the average time stamp of that dive. Can be used for
        plotting where time_average_per_dive is set as the x-axis.
    """
    from pandas import Series
    from numpy import datetime64, array
    from xarray.coding.times import decode_cf_datetime

    time = array(time)
    dives = array(dives)
    if isinstance(time[0], datetime64):
        t = time.astype(float) / 1e9
    else:
        t = time

    t_num = Series(t).groupby(dives).mean()
    t_d64 = decode_cf_datetime(t_num, time_units)
    t_ser = Series(t_d64, index=t_num.index.values)
    t_ful = t_ser.reindex(index=dives).values

    return t_ful


def mask_to_depth_array(dives, depth, var):
    """
    To be used when function returns a boolean section (as a mask) and you would
    like to return the depth of the positive mask (True) for each dive. This is
    useful for cases like MLD which returns a mask. Note that this is for
    ungridded data in "series" format.

    Parameters
    ----------
    dives : np.array, dtype=float, shape=[n, ]
        discrete dive numbers (down = d.0; up = d.5) that matches depth and var
        length
    depth : np.array, dtype=float, shape=[n, ]
        depth of each measurement
    var : np.array, dtype=bool, shape=[n,]
        mask array

    """

    from numpy import r_, diff, array
    from pandas import Series
    i = r_[False, diff(var)].astype(bool)
    idx_depth = Series(array(depth)[i], index=array(dives)[i])

    return idx_depth


def merge_dimensions(df1, df2, interp_lim=3):
    """
    Merges variables measured at different time intervals. Glider data may be
    sampled at different time intervals, as is the case for primary CTD and
    SciCon data.

    Parameters
    ----------
    df1 : pandas.DataFrame
        A dataframe indexed by datetime64 sampling times. Can have multiple
        columns. The index of this first dataframe will be preserved.
    df2 : pandas.DataFrame
        A dataframe indexed by datetime64 sampling times. Can have multiple
        columns. This second dataframe will be interpolated linearly onto the
        first dataframe.

    Returns
    -------
    merged_df : pandas.DataFrame
        The combined arrays interpolated onto the index of the first axis

    Raises
    ------
    Userwarning
        If either one of the indicies are not datetime64 dtypes

    Example
    -------
    You can use the following code and alter it if you want more control

    >>> df = pd.concat([df1, df2], sort=True, join='outer')  # doctest: +SKIP
    >>> df = (df
              .sort_index()
              .interpolate(limit=interp_lim)
              .bfill(limit=interp_lim)
              .loc[df1.index]
        )
    """

    import numpy as np
    import pandas as pd

    def isdate(df):
        return isinstance(df.index.values[0], (np.datetime64, type(pd.NaT)))

    if isdate(df1) & isdate(df2):
        df = pd.concat([df1, df2], sort=True, join='outer').sort_index()
        df = df.interpolate(limit=interp_lim).bfill(limit=interp_lim)
        return df.loc[df1.index]
    else:
        raise UserWarning('Both dataframe indicies need to be np.datetime64')


if __name__ == '__main__':

    pass
