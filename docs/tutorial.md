Publication Tutorial
====================
This tutorial accompanies the publication that we're writing.


Part 1: Importing and cleaning with physics data
------------------------------------------------

Sarah and Marcel.
Plots and code here. Don't go overboard. We have the [Examples and SOP](#) for that.


Part 2: Fluorescence calibration
--------------------------------

This Tommy's baby! show how we can calibrate by density rather than depth.


Part 3: Gridding data
---------------------
Show how we can grid to the resolution of the data.
