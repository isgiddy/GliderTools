#!/usr/bin/env python
from __future__ import (print_function as _pf,
                        unicode_literals as _ul,
                        absolute_import as _ai)

from setuptools import setup, find_packages
import os


def read(fname):
    fpath = os.path.join(os.path.dirname(__file__), fname)
    return open(fpath).read()


def walker(base, *paths):
    file_list = set([])
    cur_dir = os.path.abspath(os.curdir)

    os.chdir(base)
    try:
        for path in paths:
            for dname, _, files in os.walk(path):
                for f in files:
                    file_list.add(os.path.join(dname, f))
    finally:
        os.chdir(cur_dir)

    return list(file_list)


setup(
    # Application name:
    name="glider_tools",

    # Version number (initial):
    version="0.6",

    # Application author details:
    author="Luke Gregor",
    author_email="luke.gregor@usys.ethz.ch",

    # Packages
    packages=find_packages(),

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="https://gitlab.com/socco/GliderTools",

    license="MIT License",
    description='Tools and utilities for buoyancy glider data processing. ',
    organisation='Council for Scientific and Industrial Research',

    long_description="This is temporary. Full description will be added later",

    # Dependent packages (distributions)
    install_requires=[
        "matplotlib",
        "seawater",
        "tqdm",
        "scipy",
        "numpy",
        "xarray",
        "pandas",
        "astral",
        "netCDF4",
        "numexpr",
        "plotly",
        "gsw",
    ],
)
