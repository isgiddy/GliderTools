#!/usr/bin/env python
from __future__ import (print_function as _pf,
                        unicode_literals as _ul,
                        absolute_import as _ai)

from .seaglider import seaglider_basestation_netCDFs
from .slocum import slocum_geomar_matfile
from .seaglider import SeaGlider
