# Physical variables
Note that this summary carries on from the _Cleaning_ page.

## Deriving secondary physical variables

### Mixed Layer Depth


```python
mld = gt.physics.mixed_layer_depth(dives, depth, temp)
mld.rolling(10).mean().plot()
ylim(80,0)
show()
```


![png](img/output_31_0.png)


### Brunt-Vaisala Frequency


```python
bvf = gt.physics.brunt_vaisala(sg542.salt_qc, sg542.temp_qc, sg542.pressure)
gt.plot(sg542.dives, sg542.depth, bvf, cmap=cm.viridis, vmin=0, vmax=1.4e-4)
ylim(400,0)
show()
```


![png](img/output_33_0.png)


### Potential Density


```python
dens0 = gt.physics.potential_density(sg542.salt_qc, sg542.temp_qc, sg542.pressure, sg542.latitude, sg542.longitude)
gt.plot(sg542.dives, sg542.depth, dens0, cmap=cmo.dense)
xlim(50,150)
show()
```


![png](img/output_35_0.png)
