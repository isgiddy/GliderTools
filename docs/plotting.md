# Gridding data

## Select the appropriate bin sizes

There is a function under plot that shows the 2D histogram of the depth difference between measurements.
This can be used to estimate an informed depth bin.


```python
gt.plot.bin_size(sg542.depth, cmap=mpl.cm.CMRmap_r)
show()
```


![png](img/output_82_0.png)


## Create the bin and grid the data

Create a custom bin range with `np.arange` as shown below. The data can then be gridded. The output can be returned as a pandas.DataFrame or an xarray.DataArray (this has to be defined).


```python
custom_bin = np.r_[
    np.arange(0, 100, 0.5),
    np.arange(100, 400, 1.0),
    np.arange(400, 1000, 2.0)]

flr_gridded = gt.grid_data(x, y, flr, bins=custom_bin, as_xarray=True)

# The plot below is the standard plotting procedure for an xarray.DataArray
gt.plot(flr_gridded.interpolate_na(dim='level_1', limit=1), cmap=cmo.delta)
ylim(200, 0)
```



![png](img/output_84_1.png)
