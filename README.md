Glider Tools
============

[![DOI](https://zenodo.org/badge/141922866.svg)](https://zenodo.org/badge/latestdoi/141922866)


This readme needs to be rewritten once the paper has been structured and the documentation is public.

This is a tool to read and process Seaglider and Slocum data from basestation netCDF and MATLAB formats respectively.
For more information see the documentation of the main variable processing functions with the `calc_` prefix.
Below is a short example of how to use the data to read in and process variables.


Example Usage
-------------
This package is meant to be used in an interactive environment - ideally Jupyter Notebook

```python

	%pylab inline
	import glider_tools as gt

	sg543 = gt.seaglider.load_basestation_netCDF_files('path_to_data/p5430*.nc')

	# Load all variables needed for processing (excluding time, position and depth dimensions)
	sg543.load_multiple_vars([
		'temperature',
		'salinity',
		'pressure',
		'eng_wlbb2flvmt_Chlsig',
		'eng_wlbb2flvmt_wl470sig',
		'eng_wlbb2flvmt_wl700sig',
		'eng_qsp_PARuV',
	])

	# DataFrame of science variables with dimension `sg_data_point`
	df = sg543.data['sg_data_point']

	# Remove outliers and smooth temperature and salinity
	temp = gt.calc_physics(df.temperature, df.dives, df.depth)
	salt = gt.calc_physics(df.salinity, df.dives, df.depth)

	# Backscattering processing and despiking
	bb1, bb1_spikes, fig1 = gt.calc_bb1(df.eng_wlbb2flvmt_wl470sig, temp, salt, df.dives, df.depth, 50, 1.29e-5)
	bb2, bb2_spikes, fig2 = gt.calc_bb2(df.eng_wlbb2flvmt_wl700sig, temp, salt, df.dives, df.depth, 50, 3.50e-6)

	# Get theoretical PAR curve from exponential curve fit
	par = gt.calc_par(df.eng_qsp_PARuV, df.dives, df.depth, t, 6.678e-4, 10.6)

	# Despike fluorescence and correct for quenching
	flr_qnch, flr, qnch_layer, figs = gt.calc_fluorescence(
		df.eng_wlbb2flvmt_Chlsig, bb1, par,
		df.dives, df.depth, df.time, df.latitude, df.longitude,
		50, 0.0173, return_figure=True)

	# See the demo file for more info
```


About
-----
This work was funded by the CSIR (where Luke was working at the time of writing the code).

- Version: 0.4.2
- Author:  Luke Gregor, Tommy Ryan-Keogh
- Email:   lukegre@gmail.com
- Date:    2018-11-12
- Institution: Council for Scientific and Industrial Research
- Research group: Southern Ocean Carbon - Climate Observatory (SOCCO)

Please use the guidlines given on https://integrity.mit.edu/handbook/writing-code to cite this code.

**Example citation:**
Source: buoyancy_glider_utils [https://gitlab.com/socco/BuoyancyGliderUtils](https://gitlab.com/socco/BuoyancyGliderUtils) retrieved on 15 July 2018.



Acknowledgements
----------------
- ``ion_functions.data.flo_functions``: Christopher Wingard, Craig Risien, Russell Desiderio
- Authors of Thomalla et al. (2018)
- The SOCCO team for feedback and bug reporting
- gibbs seawater package and seawater packages are used in Glider Tools


To Do
-----
- Integrate the EGO gliders NetCDF format (http://dx.doi.org/10.13155/34980)
- Add the concept of data provenence and processing tracking to the package.
  This may require files to be returned as NetCDF from the processing functions
