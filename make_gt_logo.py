#!/usr/bin/env python
from __future__ import (print_function as _pf,
                        unicode_literals as _ul,
                        absolute_import as _ai)

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns


def main():

    x, y = profile_dummy_data()

    props = dict(transparent=True, dpi=200, bbox_inches='tight')

    fig1, ax1 = logo_with_name(x, y)
    fig2, ax2 = logo_wo_name(x, y)

    fig1.savefig('./docs/img/logo_with_name.png', **props)
    fig2.savefig('./docs/img/logo_wo_name.png', dpi=200, facecolor='#3a3a3a')


def profile_dummy_data(n_zigzags=3):

    sns.set_palette("Spectral", n_colors=n_zigzags*2)

    zigzags = np.r_[
        np.linspace(-1, -.03, 100),
        np.linspace(-.03, -1, 100)
    ].tolist() * (n_zigzags -1)

    y = np.array(
        np.linspace(0, -1, 100).tolist() +
        zigzags +
        np.linspace(-1, 0, 100).tolist()
    )

    x = np.linspace(0, 5, y.size)

    # noise = np.random.normal(loc=0, scale=0.2, size=y.size)
    # wieght = np.linspace(1, 0, y.size)
    # x += noise * wieght

    x = pd.Series(x).rolling(10, center=True).mean()
    y = pd.Series(y).rolling(10, center=True).mean()

    return x, y


def logo_with_name(x, y, interval=100, ax=None):

    if ax is None:
        fig = plt.figure(figsize=[5, 1.4])
        ax = fig.add_axes([0, 0, 0.45, 1])

    for i in range(0, x.size, interval):
        j = i + interval
        ax.scatter(x[i:j], y[i:j], 84, np.ones(interval) * i,
                   cmap=plt.cm.Spectral_r, vmin=0, vmax=x.size)

    ax.set_xticks([])
    ax.set_yticks([])
    ax.axis('off')

    fig.text(0.45, 0.46, 'GLIDER\nTOOLS',
             weight=900, size=50, color='#606060',
             ha='left', va='center')

    return fig, ax


def logo_wo_name(x, y, interval=100, ax=None):

    if ax is None:
        fig = plt.figure(figsize=[2.5, 2.5])
        ax = fig.add_axes([0, 0.2, 1, 0.6], facecolor='#3a3a3a')

    for i in range(0, x.size, interval):
        j = i + interval
        ax.scatter(x[i:j], y[i:j], 84, np.ones(interval) * i,
                   cmap=plt.cm.Spectral_r, vmin=0, vmax=x.size)

    ax.set_xticks([])
    ax.set_yticks([])
    ax.axis('off')

    c = '#3a3a3a'
    ax.set_fc(c)

    return fig, ax


if __name__ == '__main__':
    main()
