Journal Requirements
====================

There are two sources for info: Author guidelines and Article types. They contridict each oother slightly. Specifically when it comes to the word counts.


Author Guidlines
----------------
From web page https://www.frontiersin.org/journals/marine-science#author-guidelines

- Abstract word count: 250 words
- Total word count 3000 words
- Figure count: 3

The code should be novel and presented in human-readable format, adhere to the standard conventions of the language used (variable names, indentation, style and grammar), be well documented (comments in source), be provided with an example data set to show efficacy, be compilable or executable free of errors (stating configuration of system used).

The code should only call standard (freely accessible) libraries or include required libraries, and include a detailed description of the use-scenarios, expected outcomes from the code and known limitations of the code.

Please therefore make sure to provide access to the following upon submission:

1. Abstract explicitly including the language of code
2. Keywords including the language of the code in the following format:"code:language"” e.g.: "code:matlab"
3. Contribution to the field statement including the utility of the code and its language
4. Main Text including:
    - code description
    - application and utility of the code
    - link to an accessible online code repository where the most recent source code version is stored and curated (with an associated DOI for retrieval after review): https://forum.gitlab.com/t/how-to-create-doi-digital-object-identifier-for-gitlab-com-repository/3749
    - access to test data and readme files (www.zenodo.com)
    - methods used
    - example of use
    - known issues
    - licensing information (Open Source licenses recommended)
5. Compressed Archive (.zip) of the reviewed version of the code as supplementary material (.zip archives are currently available under the “Presentation” dropdown menu).


Article Types: Technology and Code
----------------------------------
From page https://www.frontiersin.org/journals/marine-science#article-types
In disagreement with the guidlines above about how long the article should be

Technology & Code articles present new technology, code and/or software or a new application of a known technology or software. This article type aims to open new avenues for theoretical and experimental investigation, data analysis or data reduction within the field of study. Technology & Code articles can also feature studies that implement existing algorithms under novel settings. Technology & Code articles are peer-reviewed, have a maximum word count of 12,000 and may include up to 15 Figures/Tables. Authors are required to pay a fee (A-type article) to publish Technology & Code article. Technology & Code articles related to innovative software solutions and/or design should be novel, presented in a well-documented, human-readable format and should be placed online in a repository, with an associated DOI/URI for retrieval. To better support the code documentation, authors can also upload a metadata file in different formats (i.e. JSON-LD, Microdata, RDFa) that incorporates all the relevant information. Authors can refer to the schema.org vocabulary, and to the SoftwareApplication/SoftwareSourceCode and Dataset related specifications. Technology & Code articles should have the following format:

1) Abstract,
2) Introduction
3) Method (including any code description)
4) Results (including examples of use and limitations)
5) Discussion (including scalability and limitations)

The following information must also be included:
- project link (e.g. sourseforge, github),
- operating system (e.g. Windows, Linux, platform independent),
- programming language (e.g. Python),
- any restrictions for non-academic use (e.g. licence needed).
