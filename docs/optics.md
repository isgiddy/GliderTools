# Optics

## Backscatter


```python
bb_raw = pd.Series(sg542.bb700_raw.copy())
dives = array(sg542.dives)
depth = array(sg542.depth)
tempC = array(sg542.temp_qc)
salt = array(sg542.salt_qc)

x = array(dives)
y = array(depth)
theta = 124
xfactor = 1.076

gt.plot(x, y, bb_raw, cmap=cmo.delta, vmin=60, vmax=200)
xlim(200,340)
title('Original Data')
show()
```


![png](img/output_37_0.png)


### Outlier bounds method


```python
ll, ul = gt.cleaning.outlier_bounds_iqr(bb_raw, multiplier=3)
mask = (bb_raw < ll) | (bb_raw > ul)
z = bb_raw.where(~mask)

ll, ul = gt.cleaning.outlier_bounds_std(bb_raw, multiplier=3)
mask = (bb_raw < ll) | (bb_raw > ul)
z2 = bb_raw.where(~mask)

gt.plot(x, y, z, cmap=cmo.delta, robust=True)
title('Outlier Bounds IQR Method')
xlim(200,340)
gt.plot(x, y, z2, cmap=cmo.delta, robust=True)
title('Outlier Bounds Stdev Method')
xlim(200,340)
show()
```


![png](img/output_39_0.png)



![png](img/output_39_1.png)


### Removing bad profiles
This function masks bad dives based on mean + std x [1] or median + std x [1] at a reference depth.


```python
bad_profiles = gt.optics.find_bad_profiles(dives, depth, z, ref_depth=300, stdev_multiplier=1, method='median')

gt.plot(x, y, z, cmap=cmo.delta, robust=True)
xlim(90,120)
title('Bad Profiles Included')

z[bad_profiles[0]] = nan
gt.plot(x, y, z, cmap=cmo.delta, robust=True)
xlim(90,120)
title('Bad Profiles Discarded')

show()
```


![png](img/output_41_0.png)



![png](img/output_41_1.png)


### Conversion from counts to total backscatter

The scale and offset function uses the factory calibration dark count and scale factor.

The bback total function uses the coefficients from Zhang et al. (2009) to convert the raw counts into total backscatter (m-1), correcting for temperature and salinity. The $\chi$ factor and $\theta$ in this example were taken from Sullivan et al. (2013) and Slade & Boss (2015).


```python
beta = gt.flo_functions.flo_scale_and_offset(z, 49, 3.217e-5)
bbp = gt.flo_functions.flo_bback_total(beta, sg542.temp_qc, sg542.salt_qc, theta, 700, xfactor)

gt.plot(x, y, beta, cmap=cmo.delta, robust=True)
xlim(200,340)
title('$\u03B2$')
gt.plot(x, y, bbp, cmap=cmo.delta, robust=True)
xlim(200,340)
title('b$_{bp}$ (m$^{-1}$)')
show()
```


![png](img/output_43_0.png)



![png](img/output_43_1.png)


### Correcting for an in situ dark count
Sensor drift from factory calibration requires an additional correction, the calculation of a dark count in situ. This is calculated from the 95th percentile of backscatter measurements between 200 and 400m.


```python
bbp = gt.optics.backscatter_dark_count(bbp, depth)

gt.plot(x, y, bbp, cmap=cmo.delta, robust=True)
xlim(200,340)
title('b$_{bp}$ (m$^{-1}$)')
show()
```


![png](img/output_45_0.png)


### Despiking
Following the methods outlined in Briggs et al. (2011) to both identify spikes in backscatter and remove them from the baseline backscatter signal. The spikes are retained as the data can be used to address specific science questions, but their presence can decrease the accuracy of the fluorescence quenching function.


```python
bbp, bbp_spikes = gt.cleaning.despike(bbp, 7, spike_method='minmax')

gt.plot(x, y, bbp, cmap=cmo.delta, robust=True)
xlim(200,340)
title('Despiked b$_{bp}$ (m$^{-1}$)')
gt.plot(x, y, bbp_spikes, cmap=cm.Spectral_r, vmin=0, vmax=0.004)
xlim(200,340)
title('b$_{bp}$ (m$^{-1}$) spikes')
show()
```


![png](img/output_47_0.png)



![png](img/output_47_1.png)


### Adding the corrected variables to the original dataframe


```python
sg542['bbp_700'] = bbp
sg542['bbp_700_spikes'] = bbp_spikes
```

### Wrapper function demonstration
A wrapper function was also designed, which is demonstrated below with the second wavelength (700 nm). The default option is for verbose to be True, which will provide an output of the different processing steps.


```python
bbp, bbp_spikes = gt.calc_backscatter(sg542.bb470_raw, sg542.temp_qc, sg542.salt_qc, sg542.dives, sg542.depth, 470,
                                     47, 1.569e-5, spike_window=7, spike_method='minmax', iqr=3, profiles_ref_depth=300,
                                    deep_multiplier=1, deep_method='median', verbose=True)

sg542['bbp_470'] = bbp
sg542['bbp_470_spikes'] = bbp_spikes
```


    ==================================================
    bb470:
    	Removing outliers with IQR * 3: 922 obs
    	Mask bad profiles based on deep values (depth=300m)
    	Number of bad profiles = 10/685
    	Zhang et al. (2009) correction
    	Dark count correction
    	Spike identification (spike window=7)


## PAR

### PAR Scaling

This function uses the factory calibration to convert from $\mu$V to $\mu$E m$^{-2}$ s$^{-1}$.


```python
par_scaled = gt.optics.par_scaling(sg542.par_raw, 6.202e-4, 10.8)
gt.plot(x, y, sg542.par_raw, robust=True)
xlim(200,340)
ylim(100,0)
title('PAR ($\mu$V)')
gt.plot(x, y, par_scaled, robust=True)
xlim(200,340)
ylim(100,0)
title('PAR ($\mu$E m$^{-2}$ m$^{-1}$)')
show()
```


![png](img/output_54_0.png)



![png](img/output_54_1.png)


### Correcting for an in situ dark count

Sensor drift from factory calibration requires an additional correction, the calculation of a dark count in situ. This is calculated from the median of PAR measurements, with additional masking applied for values before 23:01 and outside the 90th percentile.


```python
par_dark = gt.optics.par_dark_count(par_scaled, sg542.dives, sg542.depth, sg542.time)
gt.plot(x, y, par_dark, robust=True)
xlim(200,340)
ylim(100,0)
title('PAR ($\mu$E m$^{-2}$ m$^{-1}$)')
show()
```


![png](img/output_56_0.png)


### PAR replacement

This function removes the top 5 metres from each dive profile, and then algebraically recalculates the surface PAR using an exponential equation.


```python
par_filled = gt.optics.par_fill_surface(par_dark, sg542.dives, sg542.depth, max_curve_depth=80)
par_filled[par_filled < 0] = 0
```


```python
i = sg542.dives == 110

fig, ax = subplots(1, 2, figsize=[6,6], dpi=100)

ax[0].plot(par_dark[i], depth[i], lw=0.5, marker='o', ms=5)
ax[0].plot(par_filled[i], depth[i], lw=0.5, marker='o', ms=5)
ax[0].set_ylim(100,-3)
ax[0].set_xlim(-50,1000)
ax[0].set_ylabel('Depth (m)')
ax[0].set_xlabel('PAR ($\mu$E m$^{-2}$ m$^{-1}$)')

ax[1].plot(par_filled[i] - par_dark[i], depth[i], lw=0, marker='o')
ax[1].set_ylim(100,-3)
ax[1].set_xlim(-350,350)
ax[1].set_yticklabels('')
ax[1].set_xlabel('Difference between profiles')

fig.tight_layout()
show()
```


![png](img/output_59_0.png)



```python
gt.plot(x, y, par_filled, robust=True)
xlim(200,340)
ylim(100,0)
title('PAR ($\mu$E m$^{-2}$ m$^{-1}$)')
show()
```


![png](img/output_60_0.png)


### Wrapper function demonstration


```python
par = gt.calc_par(sg542.par_raw, sg542.dives, sg542.depth, sg542.time,
                   6.202e-4, 10.8, curve_max_depth=80, verbose=True)
gt.plot(x, y, par, vmin=0, vmax=300)
ylim(100, 0)
show()
```


    ==================================================
    PAR
    	Dark correction
    	Fitting exponential curve to data



![png](img/output_62_1.png)


## Deriving additional variables

### Euphotic Depth and Light attenuation coefficient


```python
euphotic_depth, kd = gt.optics.photic_depth(par_filled, dives, depth, return_mask=False, ref_percentage=1)
```


```python
fig, ax = subplots(1, 1, figsize=[6,4], dpi=100)
p1 = plot(euphotic_depth.index, euphotic_depth, label='Euphotic Depth')
ylim(120,0)
ylabel('Euphotic Depth (m)')
xlabel('Dives')
ax2 = ax.twinx()
p2 = plot(kd.index, kd, color='orange', lw=0, marker='o', ms=2, label='K$_d$')
ylabel('K$_d$', rotation=270, labelpad=20)

lns = p1+p2
labs = [l.get_label() for l in lns]
ax2.legend(lns, labs, loc=3, numpoints=1)

show()
```


![png](img/output_66_0.png)


## Fluorescence

### Quenching Correcting Method as outlined in Thomalla et al. (2017)


```python
flr_raw = pd.Series(sg542.fluo_raw.copy())
dives = array(sg542.dives)
depth = array(sg542.depth)
tempC = array(sg542.temp_qc)
salt = array(sg542.salt_qc)

x = array(dives)
y = array(depth)

gt.plot(x, y, flr_raw, cmap=cmo.delta, robust=True)
xlim(150,300)
title('Original Data')
show()
```


![png](img/output_68_0.png)


### Outlier bounds method


```python
ll, ul = gt.cleaning.outlier_bounds_iqr(flr_raw, multiplier=3)
mask = (flr_raw < ll) | (flr_raw > ul)
z = flr_raw.where(~mask)

ll, ul = gt.cleaning.outlier_bounds_std(flr_raw, multiplier=3)
mask = (flr_raw < ll) | (flr_raw > ul)
z2 = flr_raw.where(~mask)

gt.plot(x, y, z, cmap=cmo.delta, robust=True)
title('Outlier Bounds IQR Method')
xlim(150,300)
gt.plot(x, y, z2, cmap=cmo.delta, robust=True)
title('Outlier Bounds Stdev Method')
xlim(150,300)
show()
```


![png](img/output_70_0.png)



![png](img/output_70_1.png)


### Removing bad profiles

This function masks bad dives based on mean + std x [3] or median + std x [3] at a reference depth.


```python
bad_profiles = gt.optics.find_bad_profiles(dives, depth, z, ref_depth=300, stdev_multiplier=3, method='mean')

gt.plot(x, y, z, cmap=cmo.delta, robust=True)
xlim(90,150)
title('Bad Profiles Included')

z[bad_profiles[0]] = nan
gt.plot(x, y, z, cmap=cmo.delta, robust=True)
xlim(90,150)
title('Bad Profiles Discarded')

show()
```


![png](img/output_72_0.png)



![png](img/output_72_1.png)


### Correcting for an in situ dark count

Sensor drift from factory calibration requires an additional correction, the calculation of a dark count in situ. This is calculated from the 95th percentile of fluorescence measurements between 300 and 400m.


```python
flr_dark = gt.optics.fluorescence_dark_count(z, sg542.depth)

gt.plot(x, y, flr_dark, cmap=cmo.delta, robust=True)
xlim(150,300)
show()
```


![png](img/output_74_0.png)


### Despiking


```python
flr, flr_spikes = gt.cleaning.despike(flr_dark, 11, spike_method='median')

gt.plot(x, y, flr, cmap=cmo.delta, robust=True)
xlim(150,300)
title('Despiked Fluorescence')
gt.plot(x, y, flr_spikes, cmap=cm.Spectral_r, vmin=0, vmax=30)
xlim(150,300)
title('Fluorescence spikes')
show()
```


![png](img/output_76_0.png)



![png](img/output_76_1.png)


### Quenching Correction

This function uses the method outlined in Thomalla et al. (2017), briefly it calculates the quenching depth and performs the quenching correction based on the fluorescence to backscatter ratio. The quenching depth is calculated based upon the different between night and daytime fluorescence.

The default setting is for the preceding night to be used to correct the following day's quenching (`night_day_group=True`). This can be changed so that the following night is used to correct the preceding day. The quenching depth is then found from the difference between the night and daytime fluorescence, using the steepest gradient of the {5 minimum differences and the points the difference changes sign (+ve/-ve)}.

The function gets the backscatter/fluorescence ratio between from the quenching depth to the surface, and then calculates a mean nighttime ratio for each night. The quenching ratio is calculated from the nighttime ratio and the daytime ratio, which is then applied to fluorescence to correct for quenching. If the corrected value is less than raw, then the function will return the original raw data.


```python
flr_qc, qnch_layer = gt.optics.quenching_correction(flr, sg542.bbp_470, sg542.dives, sg542.depth,
                                sg542.time, sg542.latitude, sg542.longitude,
                               sunrise_sunset_offset=1, night_day_group=True)

gt.plot(x, y, flr_qc, cmap=cmo.delta, robust=True)
xlim(150,300)
title('Quenching Corrected Fluorescence')
gt.plot(x, y, qnch_layer, cmap=cm.RdBu_r)
xlim(150,300)
ylim(100,0)
title('Quenching Layer')
show()
```


![png](img/output_78_0.png)



![png](img/output_78_1.png)


### Wrapper function


```python
flr_qnch, flr, qnch_layer, [fig1, fig2] = gt.calc_fluorescence(
    sg542.fluo_raw, sg542.bbp_700, sg542.dives, sg542.depth, sg542.time,
    sg542.latitude, sg542.longitude, 53, 0.0121,
    profiles_ref_depth=300, deep_method='mean', deep_multiplier=1,
    spike_window=11, spike_method='median', return_figure=True,
    night_day_group=False, sunrise_sunset_offset=2, verbose=True)
#close(fig1)
```


    ==================================================
    Fluorescence
    	Mask bad profiles based on deep values (ref depth=300m)
    	Number of bad profiles = 19/685
    	Dark count correction
    	Quenching correction
    	Spike identification (spike window=11)
    	Generating figures for despiking and quenching report



![png](img/output_80_1.png)



![png](img/output_80_2.png)
