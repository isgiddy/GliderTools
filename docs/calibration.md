# Bottle calibration example

Bottle calibration can also be done using the `calibration` module.

The bottle file needs to be in a specific format with dates (`datetime64` format), depth and the variable values. This can be imported with any method available. I recommend `pandas.read_csv` as shown in the example below. Note that latitude and longitude are not taken into account, thus the user needs to make sure that the CTD cast was in the correct location (and time, but this will be used to match the glider).


```python
fname = '<path_to_example_dir>/calibration_SOSCEX3_PS1.csv'
cal = pd.read_csv(fname, parse_dates=['datetime'], dayfirst=True)
```

The `calibration.bottle_matchup` function returns an array that matches the size of the ungridded glider data.
The matching is done based on depth and time from both the glider and the CTD. The function will show how many samples have been matched and the smallest time difference between a CTD rosette cast and a dive (any time on the dive).


```python
sg542['bottle_sal'] = gt.calibration.bottle_matchup(
    sg542.dives, sg542.depth, sg542.time,
    cal.depth, cal.datetime, cal.sal)
```

    SUCCESS: 2016-01-05 17:46 (15 of 15 samples) match-up within 0.0 minutes
    SUCCESS: 2016-02-08 03:14 (12 of 17 samples) match-up within 0.0 minutes



```python
model = gt.calibration.robust_linear_fit(sg542.salt_qc, sg542.bottle_sal, fit_intercept=True, epsilon=1.5)
sg542['salinity_qc'] = model.predict(sg542.salt_qc)
```


![png](img/output_89_0.png)
