Loading data
============

To start using Glider Tools you first need to import the package to the
interactive workspace.

```python
# %pylab only works in IPythyon environments (including Jupyter Notebooks)
%pylab inline

import glider_tools as gt

# importing these modules is only required for finer control
import pandas as pd
from cmocean import cm as cmo  # nice colormaps in pyhton
```

Glider Tools currently supports two file formats. The first is SeaGlider
basestation dive files in `netCDF` format and the second is Slocum data prcessed
with the GEOMAR MATLAB glider tool in `mat` format.

Seaglider
---------

The base station files are first loaded, but the data is not read in. Only the metadata is fetched. The data is only loaded when the variables are loaded explicitly. This allows the user to investigate the variables before importing them.


```python
fname = '<path_to_example_data_dir>/p5420*'
dat = gt.load.seaglider_basestation_netCDFs(fname, verbose=True)
```

### Accessing individual variables

Metdata is loaded when the directory is loaded in the initialisation of the SeaGlider object. This means you can look at the properties of a variable without having to load the data (which takes a while).


```python
dat.temperature
```
```
======================================================================
Variable:        temperature
Number of Files: 343
Dimensions:      ['sg_data_point']
Coordinates:     ['ctd_time', 'latitude', 'ctd_depth', 'longitude', 'dives']
Data:            Data is not loaded
Attributes:
            units: degrees_Celsius
            comment: Termperature (in situ) corrected for thermistor first-order lag
            standard_name: sea_water_temperature
            coordinates: ctd_time longitude latitude ctd_depth
            platform: glider
```


To load the data use `data` or call `load()`. Once the data has been loaded it will not load the data again. The data object will reflect which variables have been loaded. Note that coordinate variables are loaded automatically.


```python
dat.temperature.load()
```
```
    100%|██████████| 343/343 [00:03<00:00, 97.65it/s]
```


### Load multiple variables at once

Multiple variables can also be loaded in one go. This is done using the `load_multiple_vars` function. Note that this will reload the data even if it has been loaded previously. It also returns a pandas.DataFrame if the given variables belong to one dimension. If the variables have more than one dimension, nothing will be returned, but you can still access this data in `dat.data[<dimension_name>]` as a pandas.DataFrame.

The example below shows variables that all belong to the same dimension (sg_data_point), thus returns a DataFrame.


```python
sg542 = dat.load_multiple_vars([
    'temperature',
    'salinity',
    'density',
    'pressure',
    'eng_wlbb2flvmt_Chlsig',
    'eng_wlbb2flvmt_wl470sig',
    'eng_wlbb2flvmt_wl700sig',
    'eng_qsp_PARuV',
    'eng_aa4330_O2'
]).rename(columns={
    'ctd_depth': 'depth',
    'ctd_time': 'time',
    'ctd_time_raw': 'time_raw',
    'salinity':'salinity_raw',
    'temperature':'temperature_raw',
    'eng_wlbb2flvmt_Chlsig': 'fluo_raw',
    'eng_wlbb2flvmt_wl470sig': 'bb470_raw',
    'eng_wlbb2flvmt_wl700sig': 'bb700_raw',
    'aanderaa4330_dissolved_oxygen': 'oxygen_raw',
    'eng_qsp_PARuV': 'par_raw',
})
```

```
DIMENSION: sg_data_point
[temperature, salinity, density, pressure, eng_wlbb2flvmt_Chlsig,  eng_wlbb2flvmt_wl470sig, eng_wlbb2flvmt_wl700sig, eng_qsp_PARuV, eng_aa4330_O2,  ctd_depth, latitude, longitude, ctd_time, dives]


100%|██████████| 343/343 [00:03<00:00, 103.27it/s]
```

Slocum
------
