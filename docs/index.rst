=====================================
Glider Tools: profile data processing
=====================================

Glider tools is a Python package designed to process data from the first level of processing to a science ready dataset.
The package is designed to easily import data to a standard column format (``numpy.ndarray`` or ``pandas.DataFrame``).
Cleaning and smoothing functions are flexible and can be applied as required by the user.
We provide examples and demonstrate best practices as developed by the `SOCCO Group <http://www.socco.org.za/>`_.
For the original publication of this package see: `link to the paper <#>`_.


.. toctree::
   :maxdepth: 2
   :caption: Getting started

   why
   installation
   tutorial
   cheatsheet

.. toctree::
   :maxdepth: 2
   :caption: Examples and SOP

   loading
   cleaning
   physics
   optics
   plotting
   calibration

.. toctree::
   :maxdepth: 2
   :caption: Help and Reference

   api
   citing
   contributing
   history
