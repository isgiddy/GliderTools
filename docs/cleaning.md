# Cleaning
Note that this summary carries on from the _Loading data_ page.

## Original Data

```python
dives = sg542.dives
depth = sg542.depth
x = array(dives)
y = array(depth)
salt = sg542.salinity_raw

gt.plot(x, y, salt, cmap=cmo.haline, robust=True)
xlim(50,150)
title('Original Data')
show()

```

## Filtering

### Outlier Bounds (IQR & STD)

```python
ll, ul = gt.cleaning.outlier_bounds_iqr(salt, multiplier=1.5)
mask = (salt < ll) | (salt > ul)
z = salt.where(~mask)

ll, ul = gt.cleaning.outlier_bounds_std(salt, multiplier=1.5)
mask = (salt < ll) | (salt > ul)
z2 = salt.where(~mask)

gt.plot(x, y, z, cmap=cmo.haline, robust=True)
title('Outlier Bounds IQR Method')
xlim(50,150)
gt.plot(x, y, z2, cmap=cmo.haline, robust=True)
title('Outlier Bounds Stdev Method')
xlim(50,150)
show()
```	

### Horizontal differential outliers and mask bad dive fraction

```python
m = gt.cleaning.horizontal_diff_outliers(x, y, z, depth_threshold=400, mask_frac=0.2)
z, m = gt.cleaning.mask_bad_dive_fraction(m, x, z, mask_frac=0.2)

gt.plot(x, y, z, cmap=cmo.haline)
title('Horizontal Differential Outliers removed')
xlim(50,150)
show()
```

### Removing outliers with blob detection (with convolution)

```python
i = sg542.dives > 0
mask, fig = gt.cleaning.data_density_filter(salt.where(i), y, min_count=5, conv_matrix=[21, 41])
salt_ = salt.where(~mask)
gt.plot(x, y, salt_, cmap=cmo.haline)
```

```python
i = sg542.dives > 0
mask, fig = gt.cleaning.data_density_filter(salt.where(i), y, min_count=3, conv_matrix=[11,11])
salt = salt.where(~mask)
gt.plot(x, y, salt.where(mask), cmap=cmo.haline)
```

## Despiking and smoothing

### Despike

```python
z, s = gt.cleaning.despike(z, window_size=3, spike_method='minmax')
z2, s2 = gt.cleaning.despike(z, window_size=3, spike_method='median')

gt.plot(x, y, z, cmap=cmo.haline)
title('Despiked using minimum & maximum filter')
xlim(50,150)
gt.plot(x, y, z2, cmap=cmo.haline)
title('Despiked using median filter')
xlim(50,150)
show()
```

### Rolling window

```python
z = gt.cleaning.rolling_window(z, median, window=3)

gt.plot(x, y, z, cmap=cmo.haline)
title('Median Rolling Window with size 3')
xlim(50,150)
gt.plot(x, y, sg542.salinity_raw - z, cmap=cm.RdBu, vmin=-6e-3, vmax=6e-3)
title('Difference from Original')
xlim(50,150)
show()
```

### Savitzky-Golay

```python
z = gt.cleaning.savitzky_golay(z, window_size=11, order=2)

gt.plot(x, y, z, cmap=cmo.haline)
title('Smoothing the data with Savitzky-Golay')
xlim(50,150)
gt.plot(x, y, sg542.salinity_raw - z, cmap=cm.RdBu, vmin=-6e-3, vmax=6e-3)
title('Difference from Original')
xlim(50,150)
show()
```

## Cleaning Wrapper

### Temperature
Wrapper functions have been designed to make this process more efficient, which is demonstrated below with temperature.

```python
temp = gt.calc_physics(sg542.temperature_raw, x, y, 
                        spike_window=3, spike_method='minmax',
                        iqr=1.5, savitzky_golay_window=11, savitzky_golay_order=2)

gt.plot(x, y, sg542.temperature_raw, cmap=cmo.thermal)
title('Original Data')
gt.plot(x, y, temp, cmap=cmo.thermal)
title('Cleaned Data')
gt.plot(x, y, sg542.temperature_raw - temp, cmap=cm.RdBu, vmin=-0.03, vmax=0.03)
title('Difference from Original')
show()
```

### Salinity

```python
salt = gt.calc_physics(sg542.salinity_raw, x, y, 
                        spike_window=3, spike_method='minmax',
                        iqr=1.5, savitzky_golay_window=11, savitzky_golay_order=2)

gt.plot(x, y, sg542.salinity_raw, cmap=cmo.haline)
title('Original Data')
gt.plot(x, y, salt, cmap=cmo.haline)
title('Cleaned Data')
gt.plot(x, y, sg542.salinity_raw - salt, cmap=cm.RdBu, vmin=-0.03, vmax=0.03)
title('Difference from Original')
show()
```

### Insert cleaned variabiles into the original dataframe

```python 
sg542['temp_qc'] = temp
sg542['salt_qc'] = salt
```

### Profile analysis

```python
fig, ax = subplots(1, 2, figsize=[8, 6])
fig.subplots_adjust(wspace=0.3)

dive_no = 148

idx = sg542.dives==dive_no

for i in range(2):
    ax[i].plot(z_raw[idx], y[idx], label='Raw', lw=3)
    ax[i].plot(z_iqr[idx], y[idx], label='IQR 1.5')
    ax[i].plot(z_ds[idx], y[idx], label='Despike window = 3')
    ax[i].plot(z_rw[idx], y[idx], label='Rolling Window = 3')
    ax[i].plot(z_sg[idx], y[idx], label='Savitsky-Golay', c='k', lw=1.5)
    
    ax[i].set_xlim(34.1, 34.45)

ax[1].legend(loc=1, frameon=False)

ymin, ymax= 50, 200

ax[0].fill_between([33, 35], [ymin, ymin], [ymax, ymax], facecolor='k', alpha=0.2)

ax[0].set_ylim(1000, 0)
ax[1].set_ylim(200, 50)


ax[0].set_ylabel('Depth [m]', labelpad=15)
ax[0].set_xlabel('Salinity', labelpad=15)
ax[1].set_xlabel('Salinity', labelpad=15)

ax[1].yaxis.set_ticks([50, 100, 150, 200])

ax[0].set_title('Profile ' + str(dive_no))
```