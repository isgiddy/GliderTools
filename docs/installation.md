Installation
============

Install the package with pip::

    $ pip install GliderTools

OR

Download and install with the latest updates:

    $ git clone git@gitlab.com:socco/GliderTools.git
    $ cd GliderTools
    $ python setup.py install
