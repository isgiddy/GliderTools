# -*- coding: utf-8 -*-
#!/usr/bin/env python
from __future__ import (print_function as _pf,
                        unicode_literals as _ul,
                        absolute_import as _ai)
import numpy as _np


def bottle_matchup(gld_dives, gld_depth, gld_time,
                   btl_depth, btl_time, btl_values,
                   min_depth_diff_metres=5, min_time_diff_minutes=120):
    """
    Performs a matchup between glider and bottle samples.

    Matchups are based on time and depth (or density).

    Parameters
    ----------
    gld_depth : np.array, dtype=float
        glider depth at time of measurement
    gld_dives : np.array, dtype=float
        dive index of the glider (given by glider toolbox)
    gld_time : np.array, dtype=datetime64
        glider time that will be used as primary indexing variable
    btl_time: np.array, dtype=datetime64
        in-situ bottle sample's time
    btl_depth : np.array, dtype=float
        depth of in-situ sample
    btl_values : np.array, dtype=float
        the value that will be interpolated onto the glider time and
        depth coordinates (time, depth/dens)
    min_depth_diff_metres : float, default=5
        the minimum allowable depth difference
    min_time_diff_minutes : float, default=120
        the minimum allowable time difference between bottles and glider

    Returns
    -------
    array : float
        Returns the bottle values in the format of the glider
        i.e. the length of the output will be the same as gld_*

    """

    # make all input variables np.arrays
    args = gld_time, gld_depth, gld_dives, btl_time, btl_depth, btl_values
    gld_time, gld_depth, gld_dives, btl_time, btl_depth, btl_values = map(_np.array, args)

    # create a blank array that matches glider data (placeholder for calibration bottle values)
    gld_cal = _np.ones_like(gld_depth) * _np.nan

    # loop through each ship based CTD station
    for t in _np.unique(btl_time):
        # index of station from ship CTD
        btl_idx = t == btl_time
        # number of samples per station
        btl_num = btl_idx.sum()

        # string representation of station time
        t_str = str(t.astype('datetime64[m]')).replace('T', ' ')
        t_dif = abs(gld_time - t).astype('timedelta64[m]').astype(float)

        # loop through depths for the station
        if t_dif.min() < min_time_diff_minutes:
            # index of dive where minimum difference occurs
            i = _np.where(gld_dives[_np.nanargmin(t_dif)] == gld_dives)[0]
            n_depths = 0
            for depth in btl_depth[btl_idx]:
                # an index for bottle where depth and station match
                j = btl_idx & (depth == btl_depth)
                # depth difference for glider profile
                d_dif = abs(gld_depth - depth)[i]
                # only match depth if diff is less than given threshold
                if _np.nanmin(d_dif) < min_depth_diff_metres:
                    # index of min diff for this dive
                    k = i[_np.nanargmin(d_dif)]
                    # assign the bottle values to the calibration output
                    gld_cal[k] = btl_values[j]
                    n_depths += 1
            print('SUCCESS: {} ({} of {} samples) match-up within {} minutes'.format(t_str, n_depths, btl_num, t_dif.min()))

    return gld_cal


def model_figs(bottle_data, glider_data, model):
    """
    Creates the figure for a linear model fit.

    Parameters
    ----------
    bottle_data : np.array, shape=[m, ]
        bottle data with the number of matched bottle/glider samples
    glider_data : np.array, shape[m, ]
        glider data with the number of matched bottle/glider samples
    model : sklearn.linear_model object
        a fitted model that you want to test.

    Returns
    -------
    figure axes : matplotlib.Axes
        A figure showing the fit of the
    """

    from matplotlib.pyplot import subplots
    from matplotlib.offsetbox import AnchoredText
    from sklearn import metrics

    x = bottle_data
    y = glider_data
    y_hat = model.predict(x).squeeze()
    m_name = 'Huber Regresion'

    xf = _np.linspace(x.min(), x.max(), 100)[:, None]

    fig, ax = subplots(1, 1, figsize=[6, 5], dpi=120)
    ax.plot(x, y, 'o', c='k', label='Samples')[0]

    if hasattr(model, 'outliers_'):
        ol = model.outliers_
        ax.plot(x[ol], y[ol], 'ow', mew=1, mec='k', label='Outliers ({})'.format(ol.sum()))
    else:
        ol = _np.zeros_like(y).astype(bool)

    formula = '$f(x) = {:.2g}x + {:.2g}$'.format(model.coef_[0], model.intercept_)
    ax.plot(xf, model.predict(xf), c='#AAAAAA', label='{}'.format(formula))

    ax.legend(fontsize=12, loc=2)

    params = model.get_params()
    rcModel = model.__class__().get_params()
    for key in rcModel:
        if rcModel[key] == params[key]:
            params.pop(key)

    max_len = max(len(str(params[p]))for p in params) + 1
    placeholder = u"{{}}:{{: >{}}}\n".format(max_len)
    params_str = "{} Params\n".format(m_name)
    line1_len = len(params_str)
    for key in params:
        params_str += placeholder.format(key, params[key])

    params_str += "{}\nResults (robust)\n".format('-' * (line1_len-1))
    r2_str = "$r^2$ score: {:.2g} ({:.2g})\n"
    rmse_str = "RMSE: {:.2g} ({:.2g})"

    r2_all = metrics.r2_score(y, y_hat)
    r2_robust = metrics.r2_score(y[~ol], y_hat[~ol])
    rmse_all = metrics.mean_squared_error(y, y_hat)**0.5
    rmse_robust = metrics.mean_squared_error(y[~ol], y_hat[~ol])**0.5
    params_str += r2_str.format(r2_all, r2_robust)
    params_str += rmse_str.format(rmse_all, rmse_robust)
    anchored_text = AnchoredText(params_str, loc=4,
                                 prop=dict(size=12), frameon=True)
    anchored_text.patch.set_boxstyle("round, pad=0.3, rounding_size=0.2")
    anchored_text.patch.set_linewidth(0.2)
    ax.add_artist(anchored_text)
    ax.set_ylabel('Bottle sample')
    ax.set_xlabel('Glider sample')
    ax.set_title('Calibration curve using {}'.format(m_name))

    # ax.set_ylim(0, ax.get_ylim()[1])

    return ax


def robust_linear_fit(gld_var, gld_var_cal, interpolate_limit=3,
                      return_figures=True, **kwargs):
    """
    Perform a robust linear regression using a Huber Loss Function to remove
    outliers. The

    Parameters
    ----------
    gld_var : np.array, shape=[n, ]
        glider variable
    gld_var_cal : np.array, shape=[n, ]
        bottle variable on glider indicies
    fit_intercept : bool, default=False
        forces 0 intercept if False
    return_figures : bool, default=True
        create figure with metrics
    interpolate_limit : int, default=3
        glider data may have missing points. The glider data is thus
        interpolated to ensure that as many bottle samples as possible have a
        match-up with the glider.
    **kwargs : keyword=value pairs
        will be passed to the Huber Loss regression to adjust regression

    Returns
    -------
    model : sklearn.linear_model
        A fitted model. Use model.predict(glider_var) to create the calibrated
        output.


    """

    from sklearn import linear_model
    from pandas import Series
    from numpy import log

    # make all input arguments numpy arrays
    args = gld_var, gld_var_cal
    gld_var, gld_var_cal = map(_np.array, args)

    gld_var = Series(gld_var).interpolate(limit=interpolate_limit).values

    # get bottle and glider values for the variables
    i = ~_np.isnan(gld_var_cal) & ~_np.isnan(gld_var)
    y = gld_var_cal[i]
    x = gld_var[i][:, None]

    if "fit_intercept" not in kwargs:
        kwargs["fit_intercept"] = False
    model = linear_model.HuberRegressor(**kwargs)


    model.fit(x, y)

    if return_figures:
        model_figs(x, y, model)

    model._predict = model.predict

    def predict(self, x):
        """
        A wrapper around the normal predict function that takes
        nans into account. An extra dimension is also added if needed.
        """
        x = _np.array(x)
        out = _np.ndarray(x.size) * _np.NaN
        i = ~_np.isnan(x)
        if len(x.shape) != 2:
            x = x[:, None]
        out[i] = self._predict(x[i]).squeeze()

        return out

    model.predict = predict.__get__(model, linear_model.HuberRegressor)

    return model
